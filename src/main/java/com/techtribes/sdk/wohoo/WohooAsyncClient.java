package com.techtribes.sdk.wohoo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.boot.web.client.RestTemplateBuilder;

import com.techtribes.sdk.wohoo.model.request.OrderHandlingRequest;
import com.techtribes.sdk.wohoo.model.request.PlaceOrderRequest;
import com.techtribes.sdk.wohoo.model.response.CategoryDataResponse;
import com.techtribes.sdk.wohoo.model.response.CategoryResponse;
import com.techtribes.sdk.wohoo.model.response.OrderHandlingResponse;
import com.techtribes.sdk.wohoo.model.response.OrderRefnoResponse;
import com.techtribes.sdk.wohoo.model.response.PlaceOrderResponse;
import com.techtribes.sdk.wohoo.model.response.ProductDetailsResponse;
import com.techtribes.sdk.wohoo.model.response.SettingsResponse;
import com.techtribes.sdk.wohoo.resource.PathParams;

public class WohooAsyncClient implements WohooAsync {

    WohooClient client;
    ThreadPoolExecutor executorService;

    public WohooAsyncClient(RestTemplateBuilder builder, ThreadPoolExecutor executorService) {
        this.client = WohooClientBuilder.standard(builder).build();
        this.executorService = executorService;
    }

    public WohooAsyncClient(ThreadPoolExecutor executorService) {
        this.client = WohooClientBuilder.standard().build();
        this.executorService = executorService;
    }

    public WohooAsyncClient(WohooClient client, ThreadPoolExecutor executorService) {
        this.client = client;
        this.executorService = executorService;
    }

    public void enableRequestLogging(Boolean value) {
        this.client.enableRequestLogging(value);
    }

    public void enableResponseLogging(Boolean value) {
        this.client.enableResponseLogging(value);
    }

    protected void setCorePoolSize(int minThreads) {
        this.executorService.setCorePoolSize(minThreads);
    }

    public void setEndpoint(String endpoint) {
        this.client.setEndpoint(endpoint);
    }

    public void setConsumerKey(String consumerKey) {
        this.client.setConsumerKey(consumerKey);
    }

    public void setConsumerSecret(String consumerSecret) {
        this.client.setConsumerSecret(consumerSecret);
    }

    public void setAccessToken(String accessToken) {
        this.client.setAccessToken(accessToken);
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.client.setAccessTokenSecret(accessTokenSecret);
    }

    @Override
    public Future<SettingsResponse> getSettings() throws WohooClientException {
        return CompletableFuture.supplyAsync(() -> this.client.getSettings(), executorService);
    }

    @Override
    public Future<CategoryResponse> getCategory() throws WohooClientException {
        return CompletableFuture.supplyAsync(() -> this.client.getCategory(), executorService);
    }

    @Override
    public Future<CategoryDataResponse> getCategoryData(PathParams pathParams) throws WohooClientException {
        return CompletableFuture.supplyAsync(() -> this.client.getCategoryData(pathParams), executorService);
    }

    @Override
    public Future<ProductDetailsResponse> getProductDetails(PathParams pathParams) throws WohooClientException {
        return CompletableFuture.supplyAsync(() -> this.client.getProductDetails(pathParams), executorService);
    }

    @Override
    public Future<PlaceOrderResponse> placeOrder(PlaceOrderRequest placeOrderRequest) throws WohooClientException {
        return CompletableFuture.supplyAsync(() -> this.client.placeOrder(placeOrderRequest), executorService);
    }

    @Override
    public Future<OrderHandlingResponse> getOrderHandlingCharges(OrderHandlingRequest orderHandlingRequest)
            throws WohooClientException {
        return CompletableFuture.supplyAsync(() -> this.client.getOrderHandlingCharges(orderHandlingRequest),
                executorService);
    }

    @Override
    public Future<OrderRefnoResponse> getOrderReferenceNumber() throws WohooClientException {
        return CompletableFuture.supplyAsync(() -> this.client.getOrderReferenceNumber(), executorService);
    }

}
