package com.techtribes.sdk.wohoo.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class RestTemplateLoggingInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger log = LoggerFactory.getLogger(RestTemplateLoggingInterceptor.class);

    private Boolean requestBodyLogging = true;
    private Boolean responseBodyLogging = true;
    private Boolean prettyPrint = true;
    private String logPrefix = "";

    /**
     * When set to true the request body will be logged if one exists.
     * 
     * @param value
     */
    public void enableRequestBodyLogging(Boolean value) {
        this.requestBodyLogging = value;
    }

    /**
     * When set to true the response body will be logged if one exists.
     * 
     * @param value
     */
    public void enableResponseBodyLogging(Boolean value) {
        this.responseBodyLogging = value;
    }

    /**
     * When set to true response body is pretty printed
     *
     * @param value
     */
    public void enablePrettyPrint(Boolean value) {
        this.prettyPrint = value;
    }

    /**
     * Prefix to be included in logging
     *
     * @param logPrefix
     */
    public void setLogPrefix(String logPrefix) {
        this.logPrefix = logPrefix;
    }

    /**
     * Intercept the HTTP request wrapping it in some logging and timing routines.
     */
    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

        final long start = System.currentTimeMillis();
        logRequest(request, body);
        ClientHttpResponse clientHttpResponse = execution.execute(request, body);
        logResponse(clientHttpResponse, start);
        return clientHttpResponse;
    }

    /**
     * Request log line handling.
     * 
     * @param request
     * @param body
     * @throws IOException
     */
    private void logRequest(HttpRequest request, byte[] body) throws IOException {

        log.info("HTTP::BEGIN: {} {} {} [{} bytes]", logPrefix, request.getMethod(), request.getURI(), body.length);

        printBody(getRequestBody(body), requestBodyLogging);

    }

    /**
     * Response log line handling.
     * 
     * @param response
     * @throws IOException
     */
    private void logResponse(ClientHttpResponse response, long start) throws IOException {

        final long end = System.currentTimeMillis();

        String responseMediaType = response.getHeaders().getContentType().toString();

        if (responseMediaType.contains("text/json")) {
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        }

        String body = getBodyString(response);
        log.error("HTTP :: HEADERS RestTemplate - Response Headers " + response.getHeaders());

        printBody(body, responseBodyLogging);

        log.info("HTTP::END: {} {} {} {} ms [{} bytes]", logPrefix, response.getStatusCode(), response.getStatusText(),
                end - start, body != null ? body.getBytes(StandardCharsets.UTF_8).length : "{No body to print!!!}");

    }

    /**
     * Get a UTF-8 encoded string from the request body.
     * 
     * @param body
     * @return
     * @throws UnsupportedEncodingException
     */
    private String getRequestBody(byte[] body) throws UnsupportedEncodingException {
        if (body != null && body.length > 0) {
            return (new String(body, "UTF-8"));
        } else {
            return null;
        }
    }

    /**
     * Reads a response body and turns it into a string which preserves any new line characters as appropriate.
     * 
     * @param response
     * @return
     */
    private String getBodyString(ClientHttpResponse response) {
        try {
            if (response != null && response.getBody() != null) {

                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(response.getBody(), StandardCharsets.UTF_8));

                return bufferedReader.lines().collect(Collectors.joining("\n"));

            } else {
                return null;
            }
        } catch (IOException e) {
            log.error("Unable to retrieve response body as string: {}", e.getMessage(), e);
            return null;
        }
    }

    /**
     * Print the body of the HTTP request or response
     *
     * @param body
     * @param printBodyEnabled
     */
    private void printBody(String body, Boolean printBodyEnabled) {
        if (body != null && printBodyEnabled && prettyPrint) {
            Arrays.stream(body.split("\n")).forEach(log::info);
        } else if (body != null && printBodyEnabled && !prettyPrint) {
            log.info(body);
        }
    }

}
