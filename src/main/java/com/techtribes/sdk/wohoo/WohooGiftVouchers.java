package com.techtribes.sdk.wohoo;

import com.techtribes.sdk.wohoo.model.request.OrderHandlingRequest;
import com.techtribes.sdk.wohoo.model.request.PlaceOrderRequest;
import com.techtribes.sdk.wohoo.model.response.CategoryDataResponse;
import com.techtribes.sdk.wohoo.model.response.CategoryResponse;
import com.techtribes.sdk.wohoo.model.response.OrderHandlingResponse;
import com.techtribes.sdk.wohoo.model.response.OrderRefnoResponse;
import com.techtribes.sdk.wohoo.model.response.PlaceOrderResponse;
import com.techtribes.sdk.wohoo.model.response.ProductDetailsResponse;
import com.techtribes.sdk.wohoo.model.response.SettingsResponse;
import com.techtribes.sdk.wohoo.resource.PathParams;

public interface WohooGiftVouchers {

    public SettingsResponse getSettings() throws WohooClientException;

    public CategoryResponse getCategory() throws WohooClientException;

    public CategoryDataResponse getCategoryData(PathParams pathParams) throws WohooClientException;

    public ProductDetailsResponse getProductDetails(PathParams pathParams) throws WohooClientException;

    public PlaceOrderResponse placeOrder(PlaceOrderRequest orderRequest) throws WohooClientException;

    public OrderHandlingResponse getOrderHandlingCharges(OrderHandlingRequest orderHandlingRequest)
            throws WohooClientException;

    public OrderRefnoResponse getOrderReferenceNumber() throws WohooClientException;

}
