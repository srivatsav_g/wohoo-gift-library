package com.techtribes.gv.service.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.techtribes.gv.constants.Constants.OrderStatus;
import com.techtribes.gv.service.bean.Address;
import com.techtribes.gv.service.bean.Category;
import com.techtribes.gv.service.bean.CommunicationDetail;
import com.techtribes.gv.service.bean.PaymentType;
import com.techtribes.gv.service.bean.PriceType;
import com.techtribes.gv.service.bean.Product;
import com.techtribes.gv.service.bean.UserDetail;
import com.techtribes.gv.service.mapper.WohooMapper;
import com.techtribes.gv.service.model.response.BaseConfigurationResponse;
import com.techtribes.gv.service.model.response.CategoryDataResponse;
import com.techtribes.gv.service.model.response.CategoryDetailsResponse;
import com.techtribes.gv.service.model.response.HandlingChargesResponse;
import com.techtribes.gv.service.model.response.OrderDataResponse;
import com.techtribes.sdk.wohoo.model.request.Billing;
import com.techtribes.sdk.wohoo.model.request.PaymentMethod;
import com.techtribes.sdk.wohoo.model.request.PlaceOrderRequest;
import com.techtribes.sdk.wohoo.model.request.Shipping;
import com.techtribes.sdk.wohoo.model.response.CategoryResponse;
import com.techtribes.sdk.wohoo.model.response.OrderHandlingResponse;
import com.techtribes.sdk.wohoo.model.response.PlaceOrderResponse;
import com.techtribes.sdk.wohoo.model.response.SettingsResponse;

@Component
public class WohooMapperImpl implements WohooMapper {

    public BaseConfigurationResponse toBaseCofiguation(SettingsResponse settingsResponse) {
        BaseConfigurationResponse configResponse = new BaseConfigurationResponse();
        configResponse.setStoreId(settingsResponse.getStoreId());
        configResponse.setStatus(settingsResponse.getSuccess());
        configResponse.setFaqs(settingsResponse.getFaq());
        configResponse.setMaxQuantity(settingsResponse.getConfig() != null
                ? Integer.parseInt(settingsResponse.getConfig().getProductMaxQtyAllow()) : null);
        configResponse.setPayentMethodValue(PaymentType.valueOf(settingsResponse.getPaymentMethod().getValue()));
        configResponse.setPaymentMethodName(
                settingsResponse.getPaymentMethod() != null ? settingsResponse.getPaymentMethod().getLabel() : null);
        configResponse.setTerms(settingsResponse.getTermsAndConditions());
        configResponse.setBaseUrl(settingsResponse.getBaseUrl());
        configResponse.setErrorCode(settingsResponse.getErrorCode());
        configResponse.setErrorMessage(settingsResponse.getMessage());
        return configResponse;
    }

    public CategoryDetailsResponse toCateoryDetailsResponse(CategoryResponse categoryListResponse) {

        CategoryDetailsResponse response = new CategoryDetailsResponse();

        Category mainCategory = new Category();
        List<Category> parentCategories = new ArrayList<>();
        List<Category> childCategories = new ArrayList<>();

        mainCategory.setCategoryId(categoryListResponse.getRootCategory().getCategoryId());
        mainCategory.setCategoryImage(categoryListResponse.getRootCategory().getCategoryImage());
        mainCategory.setCategoryName(categoryListResponse.getRootCategory().getCategoryName());

        response.setMainCategory(mainCategory);

        if (categoryListResponse.getRootCategory().getCategoryList() != null) {
            categoryListResponse.getRootCategory().getCategoryList().stream()
                    .filter(parentCategory -> parentCategory != null && parentCategory.getParentCategory() != null)
                    .forEach(parentCategory -> {
                        Category category1 = new Category();
                        category1.setCategoryId(parentCategory.getParentCategory().getCategoryId());
                        category1.setCategoryImage(parentCategory.getParentCategory().getCategoryImage());
                        category1.setCategoryName(parentCategory.getParentCategory().getCategoryName());
                        parentCategories.add(category1);

                        parentCategory.getParentCategory().getChildCategory().stream()
                                .filter(childCategory -> childCategory != null).forEach(childCategory -> {
                                    Category category2 = new Category();
                                    category2.setCategoryId(childCategory.getCategoryId());
                                    category2.setCategoryImage(childCategory.getCategoryImage());
                                    category2.setCategoryName(childCategory.getCategoryName());
                                    childCategories.add(category2);
                                });

                    });
        }

        response.setParentCategories(parentCategories);
        response.setChildCategories(childCategories);

        return response;
    }

    public CategoryDataResponse toCategoryDataResponse(
            com.techtribes.sdk.wohoo.model.response.CategoryDataResponse categoryDataResponse) {
        CategoryDataResponse response = new CategoryDataResponse();
        response.setStatus(categoryDataResponse.getSuccess());

        Category category = new Category();
        category.setCategoryId(categoryDataResponse.getId());
        category.setCategoryName(categoryDataResponse.getName());
        category.setCategoryImage(categoryDataResponse.getBannerImage());

        List<Category> subCategories = new ArrayList<>();
        List<Product> products = new ArrayList<>();

        response.setCategory(category);
        response.setNavigationUrlPath(categoryDataResponse.getNavigationUrlpath());
        response.setCategoryImage(categoryDataResponse.getBannerImage());

        if (categoryDataResponse.getEmbedded() != null) {
            if (categoryDataResponse.getEmbedded().getSubCategory() != null) {
                categoryDataResponse.getEmbedded().getSubCategory().stream().filter(c -> c != null).forEach(c -> {
                    Category subCategory = new Category();
                    subCategory.setCategoryId(c.getId());
                    subCategory.setCategoryName(c.getName());
                    subCategory.setCategoryImage(c.getImage());
                    subCategories.add(subCategory);
                });
                response.setSubCategories(subCategories);
            }

            if (categoryDataResponse.getEmbedded().getProduct() != null) {
                categoryDataResponse.getEmbedded().getProduct().stream().filter(p -> p != null).forEach(p -> {
                    Product product = new Product();
                    product.setAggregatorBrandId(p.getBrandId());
                    product.setAggregatorProductId(Integer.toString(p.getId()));
                    product.setCurrency("INR");
                    product.setAggregatorBrandName(p.getName());
                    product.setPriceType(PriceType.valueof(p.getPriceType()));
                    products.add(product);
                });
                response.setProducts(products);
            }
        }
        response.setErrorCode(categoryDataResponse.getErrorCode());
        response.setErrorMessage(categoryDataResponse.getMessage());
        return response;
    }

    public HandlingChargesResponse toHandlingChargesResponse(OrderHandlingResponse orderHandlingResponse) {
        HandlingChargesResponse response = new HandlingChargesResponse();
        response.setHadlingCharges(orderHandlingResponse.getHandlingAmount());
        response.setErrorCode(orderHandlingResponse.getErrorCode());
        response.setErrorMessage(orderHandlingResponse.getMessage());
        response.setStatus(orderHandlingResponse.getSuccess());
        return response;
    }

    public PlaceOrderRequest toAggregatorOrderRequest(
            com.techtribes.gv.service.model.request.PlaceOrderRequest placeOrderRequest) {

        PlaceOrderRequest aggregatorOrderRequest = new PlaceOrderRequest();

        Billing billing = toBillingAndShippingDetail(placeOrderRequest.getBillingUserDetail());
        aggregatorOrderRequest.setBilling(billing);
        Shipping shipping = toBillingAndShippingDetail(placeOrderRequest.getShippingUserDetail());
        aggregatorOrderRequest.setShipping(shipping);
        List<PaymentMethod> aggregatorPaymentDetails = new ArrayList<PaymentMethod>();
        PaymentMethod aggregatorPaymentMethod = new PaymentMethod();
        aggregatorPaymentMethod.setMethod(placeOrderRequest.getPaymentType().name());
        aggregatorPaymentMethod.setAmountToRedem(Integer.parseInt(placeOrderRequest.getAmountToRedeem()));
        // TODO: generate unique PO number.. and set in the request.
        aggregatorPaymentDetails.add(aggregatorPaymentMethod);
        aggregatorOrderRequest.setPaymentMethod(aggregatorPaymentDetails);
        // TODO: set aggregator's reference number in the request, which is generated using their API.
        List<com.techtribes.sdk.wohoo.model.request.Product> products =
                toRequestProductDetail(placeOrderRequest.getProducts());
        aggregatorOrderRequest.setProducts(products);
        return aggregatorOrderRequest;
    }

    /**
     * Method to map all products to client Product types.
     * 
     * @param products
     * @return
     */
    private List<com.techtribes.sdk.wohoo.model.request.Product> toRequestProductDetail(List<Product> products) {
        List<com.techtribes.sdk.wohoo.model.request.Product> aggregatorProducts = new ArrayList<>();
        products.stream().filter(product -> product != null).forEach(product -> {
            com.techtribes.sdk.wohoo.model.request.Product aggregatorProduct =
                    new com.techtribes.sdk.wohoo.model.request.Product();

            aggregatorProduct.setProductId(product.getAggregatorProductId());
            aggregatorProduct.setQty(Integer.toString(product.getProductQuantity()));
            // TODO: check for this price precision.
            aggregatorProduct.setPrice(Long.toString(product.getProductPrice()));
            // TODO: check if theme and gift message are required for the request.
            aggregatorProducts.add(aggregatorProduct);
        });
        return aggregatorProducts;
    }

    /**
     * Method to map Billing USer details and Shipping User Details.
     * 
     * @param userDetail
     * @return
     */
    private <T> T toBillingAndShippingDetail(UserDetail userDetail) {

        CommunicationDetail<Address> addressDetail = userDetail.getAddress();
        CommunicationDetail<String> phoneDetail = userDetail.getPhone();
        CommunicationDetail<String> mailDetail = userDetail.getEmail();

        switch (userDetail.getUserType()) {
            case BILLING_USER:
                Billing billingUserDetail = new Billing();
                billingUserDetail.setFirstname(userDetail.getFirstName());
                billingUserDetail.setLastname(userDetail.getLastName());

                billingUserDetail.setLine1(addressDetail.getDetail().getAddressLine1());
                billingUserDetail.setLine2(addressDetail.getDetail().getAddressLine2());
                billingUserDetail.setCity(addressDetail.getDetail().getCity());
                billingUserDetail.setPostcode(addressDetail.getDetail().getZip());
                billingUserDetail.setRegion(addressDetail.getDetail().getState());
                billingUserDetail.setCountryId(addressDetail.getDetail().getCountry());

                billingUserDetail.setTelephone(phoneDetail.getDetail());

                billingUserDetail.setEmail(mailDetail.getDetail());

                return (T) billingUserDetail;

            case SHIPPING_USER:
                Shipping shippingUserDetail = new Shipping();
                shippingUserDetail.setFirstname(userDetail.getFirstName());
                shippingUserDetail.setLastname(userDetail.getLastName());

                shippingUserDetail.setLine1(addressDetail.getDetail().getAddressLine1());
                shippingUserDetail.setLine2(addressDetail.getDetail().getAddressLine2());
                shippingUserDetail.setCity(addressDetail.getDetail().getCity());
                shippingUserDetail.setPostcode(addressDetail.getDetail().getZip());
                shippingUserDetail.setRegion(addressDetail.getDetail().getState());
                shippingUserDetail.setCountryId(addressDetail.getDetail().getCountry());

                shippingUserDetail.setTelephone(phoneDetail.getDetail());

                shippingUserDetail.setEmail(mailDetail.getDetail());

                return (T) shippingUserDetail;
            default:
                return null;

        }

    }

    public OrderDataResponse toOrderDataResponse(PlaceOrderResponse placeOrderResponse) {
        OrderDataResponse orderDataResponse = new OrderDataResponse();
        orderDataResponse.setIsOrderPlaced(placeOrderResponse.getSuccess());
        orderDataResponse.setOrderStatus(OrderStatus.valueOf(placeOrderResponse.getStatus()));
        orderDataResponse.setAggregatorOrderId(placeOrderResponse.getOrderId());
        orderDataResponse.setAggregatorOrderMessage(placeOrderResponse.getMessage());
        orderDataResponse.setErrorCode(placeOrderResponse.getErrorCode());
        orderDataResponse.setErrorMessage(placeOrderResponse.getMessage());
        return orderDataResponse;
    }

}
