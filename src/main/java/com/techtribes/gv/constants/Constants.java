package com.techtribes.gv.constants;

public class Constants {

    public static enum DBStatus {
        success, failure
    }

    public static enum Status {
        success, failure
    }
    public static enum OrderStatus {
        PROCESSING,SHIPPED,COMPLETE,CANCELLED,BLOCKED
    }
}
