package com.techtribes.sdk.wohoo.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PathParams {

    private List<String> params;

    public PathParams() {
        this.params = new ArrayList<>();
    }

    public PathParams(String... params) {
        this.params = new ArrayList<String>(Arrays.asList(params));
    }

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    public void addParam(String param) {
        this.params.add(param);
    }

    public void addParams(String... params) {
        (new ArrayList<String>(Arrays.asList(params))).stream().filter(param -> param == null)
                .forEach(param -> this.addParam(param));
    }

    public boolean isEmpty() {
        if (params == null || params.size() == 0) {
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return params.stream().collect(Collectors.joining("/"));
    }
}
