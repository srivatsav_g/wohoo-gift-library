package com.techtribes.sdk.wohoo.resource;

public class SimpleFilter {

    private QueryKey key;
    private String value;

    public QueryKey getKey() {
        return key;
    }

    public void setKey(QueryKey key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    SimpleFilter(QueryKey key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "SimpleFilter [key=" + key + ", value=" + value + "]";
    }

}
