package com.techtribes.gv.service.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techtribes.gv.response.BaseData;
import com.techtribes.gv.service.bean.PaymentType;

public class BaseConfigurationResponse extends BaseData {

    private Boolean status;
    private Integer storeId;
    /**
     * allowable quantity per order.
     */
    private Integer maxQuantity;
    private String paymentMethodName;
    /**
     * purchase_order
     */
    private PaymentType payentMethodValue;
    /**
     * Terms and conditions
     */
    private String terms;
    private String faqs;
    private String baseUrl;
    @JsonIgnore
    private String errorCode;
    @JsonIgnore
    private String errorMessage;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public PaymentType getPayentMethodValue() {
        return payentMethodValue;
    }

    public void setPayentMethodValue(PaymentType payentMethodValue) {
        this.payentMethodValue = payentMethodValue;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getFaqs() {
        return faqs;
    }

    public void setFaqs(String faqs) {
        this.faqs = faqs;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public String toString() {
        return "BaseConfigurationResponse [status=" + status + ", storeId=" + storeId + ", maxQuantity=" + maxQuantity
                + ", paymentMethodName=" + paymentMethodName + ", payentMethodValue=" + payentMethodValue + ", terms="
                + terms + ", faqs=" + faqs + ", baseUrl=" + baseUrl + ", errorCode=" + errorCode + ", errorMessage="
                + errorMessage + "]";
    }

}
