package com.techtribes.gv.service.model.response;

import java.util.List;

import com.techtribes.gv.response.BaseData;
import com.techtribes.gv.service.bean.Category;

public class CategoryDetailsResponse extends BaseData {

    private Category mainCategory;
    private List<Category> parentCategories;
    private List<Category> childCategories;

    public Category getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(Category mainCategory) {
        this.mainCategory = mainCategory;
    }

    public List<Category> getParentCategories() {
        return parentCategories;
    }

    public void setParentCategories(List<Category> parentCategories) {
        this.parentCategories = parentCategories;
    }

    public List<Category> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<Category> childCategories) {
        this.childCategories = childCategories;
    }

    @Override
    public String toString() {
        return "CategoryDetails [mainCategory=" + mainCategory + ", parentCategories=" + parentCategories
                + ", childCategories=" + childCategories + "]";
    }

}
