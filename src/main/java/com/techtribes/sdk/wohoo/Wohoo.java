package com.techtribes.sdk.wohoo;

import com.techtribes.sdk.wohoo.resource.PathParams;
import com.techtribes.sdk.wohoo.resource.QueryParameters;
import com.techtribes.sdk.wohoo.resource.WohooResource;

public interface Wohoo extends WohooGiftVouchers {

    default String buildUrl(String endPoint, WohooResource wohooResource, PathParams pathParams,
            QueryParameters queryParams) {

        StringBuilder urlBuilder = new StringBuilder().append(endPoint).append(wohooResource.getUri());

        if (pathParams != null && !pathParams.isEmpty()) {
            urlBuilder.append("/").append(pathParams.toString());
        }

        if (queryParams != null) {
            urlBuilder.append("?").append(queryParams.toString());
        }

        return urlBuilder.toString();

    }

    default String buidlUrl(String endPoint, WohooResource wohooResource, QueryParameters queryParams) {
        return buildUrl(endPoint, wohooResource, null, queryParams);
    }

    default String buidlUrl(String endPoint, WohooResource wohooResource, PathParams pathParams) {
        return buildUrl(endPoint, wohooResource, pathParams, null);
    }

    default String buildUrl(String endpoint, WohooResource wohooResource) {
        return buildUrl(endpoint, wohooResource, null, null);
    }

}
