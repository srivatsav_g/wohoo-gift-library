package com.techtribes.sdk.wohoo.utils;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.MDC;

/**
 * ThreadPoolExecutor that wraps the running thread in order to capture the main thread MDC context and apply it to the
 * new thread.
 * 
 * @author From https://gist.github.com/pismy/117a0017bf8459772771
 *
 */

public class MDCAwareThreadPoolExecutor extends ThreadPoolExecutor {

    public MDCAwareThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
            BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    public MDCAwareThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
            BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
    }

    public MDCAwareThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
            BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
    }

    public MDCAwareThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
            BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }

    @Override
    public void execute(Runnable task) {
        super.execute(new MDCAwareRunnableWrapper(task));
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        return super.submit(new MDCAwareCallableWrapper<>(task));
    }

    @Override
    public Future<?> submit(Runnable task) {
        return super.submit(new MDCAwareRunnableWrapper(task));
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        return super.submit(new MDCAwareRunnableWrapper(task), result);
    }

    // Helper class to capture main thread MDC map and to then use it on any
    // running threads.
    static class MDCAwareRunnableWrapper implements Runnable {

        private final Runnable wrapped;
        private final Map<String, String> map;

        public MDCAwareRunnableWrapper(Runnable wrapped) {
            this.wrapped = wrapped;
            // capture MDC of this thread (it's the main one)
            map = MDC.getCopyOfContextMap();
        }

        @Override
        public void run() {
            if (map != null) {
                MDC.setContextMap(map);
            }
            wrapped.run();
        }

    }

    // Helper class to capture main thread MDC map and to then use it on any
    // running threads.
    static class MDCAwareCallableWrapper<T> implements Callable<T> {

        private final Callable<T> wrapped;
        private final Map<String, String> map;

        public MDCAwareCallableWrapper(Callable<T> wrapped) {
            this.wrapped = wrapped;
            // capture MDC of this thread (it's the main one)
            map = MDC.getCopyOfContextMap();
        }

        @Override
        public T call() throws Exception {
            if (map != null) {
                MDC.setContextMap(map);
            }
            return wrapped.call();
        }
    }

}