package com.techtribes.gv.service.proxy;

import java.util.List;

import com.techtribes.gv.service.model.request.PlaceOrderRequest;
import com.techtribes.gv.service.model.response.BaseConfigurationResponse;
import com.techtribes.gv.service.model.response.CategoryDataResponse;
import com.techtribes.gv.service.model.response.CategoryDetailsResponse;
import com.techtribes.gv.service.model.response.HandlingChargesResponse;
import com.techtribes.gv.service.model.response.OrderDataResponse;

public interface GiftVoucherProxy {

    public BaseConfigurationResponse getSettings();

    public CategoryDetailsResponse getCategoryDetails();

    public CategoryDataResponse getCategoryData(String categoryId);

    public HandlingChargesResponse getHandlingCharges(List<String> productIds);
    
    public OrderDataResponse placeOrder(PlaceOrderRequest placeOrderRequest);
}
