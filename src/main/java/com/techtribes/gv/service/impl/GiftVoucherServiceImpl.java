package com.techtribes.gv.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techtribes.gv.constants.Constants.Status;
import com.techtribes.gv.constants.ErrorCode;
import com.techtribes.gv.response.BaseResponse;
import com.techtribes.gv.service.GiftVoucherService;
import com.techtribes.gv.service.bean.Aggregator;
import com.techtribes.gv.service.model.request.PlaceOrderRequest;
import com.techtribes.gv.service.model.response.BaseConfigurationResponse;
import com.techtribes.gv.service.model.response.CategoryDataResponse;
import com.techtribes.gv.service.model.response.CategoryDetailsResponse;
import com.techtribes.gv.service.model.response.HandlingChargesResponse;
import com.techtribes.gv.service.model.response.OrderDataResponse;
import com.techtribes.gv.service.proxy.GiftVoucherProxy;
import com.techtribes.gv.service.proxy.impl.GiftVoucherProxyFactory;

@Service
public class GiftVoucherServiceImpl implements GiftVoucherService {

    @Autowired
    private GiftVoucherProxyFactory aggregatorFactory;

    private static final Logger LOGGER = LoggerFactory.getLogger(GiftVoucherServiceImpl.class);

    private final String LOG_PREFIX = this.getClass().getSimpleName();

    @Override
    public BaseResponse<BaseConfigurationResponse> getSettings() {
        GiftVoucherProxy proxy = aggregatorFactory.getProxy(Aggregator.WOHOO);
        try {
            BaseConfigurationResponse response = proxy.getSettings();
            if (response.getStatus()) {
                return new BaseResponse<>(response, Status.success, null, null);
            } else {
                return new BaseResponse<>(response, Status.failure, ErrorCode.getErrorCode(response.getErrorCode()),
                        response.getErrorMessage());
            }
        } catch (Exception e) {
            LOGGER.error(LOG_PREFIX + " Exception in getSettings() :: " + e);
            return new BaseResponse<>(null, Status.failure, ErrorCode.TECHNICAL_ERROR, null);
        }
    }

    @Override
    public BaseResponse<CategoryDetailsResponse> getCategoryDetails() {
        GiftVoucherProxy proxy = aggregatorFactory.getProxy(Aggregator.WOHOO);
        try {
            CategoryDetailsResponse response = proxy.getCategoryDetails();
            return new BaseResponse<>(response, Status.success, null, null);
        } catch (Exception e) {
            return new BaseResponse<>(null, Status.failure, ErrorCode.TECHNICAL_ERROR, null);
        }
    }

    @Override
    public BaseResponse<CategoryDataResponse> getCategoryData(String categoryId) {
        GiftVoucherProxy proxy = aggregatorFactory.getProxy(Aggregator.WOHOO);
        try {
            CategoryDataResponse response = proxy.getCategoryData(categoryId);
            if (response.getStatus()) {
                return new BaseResponse<>(response, Status.success, null, null);
            } else {
                return new BaseResponse<>(response, Status.failure, ErrorCode.getErrorCode(response.getErrorCode()),
                        response.getErrorMessage());
            }

        } catch (Exception e) {
            return new BaseResponse<>(null, Status.failure, ErrorCode.TECHNICAL_ERROR, null);
        }
    }

    @Override
    public BaseResponse<OrderDataResponse> placeOrder(PlaceOrderRequest placeOrderRequest) {
        // TODO : Get aggregator reference number.
        GiftVoucherProxy proxy = aggregatorFactory.getProxy(Aggregator.WOHOO);
        try {
            OrderDataResponse response = proxy.placeOrder(placeOrderRequest);
            if (response.getIsOrderPlaced()) {
                return new BaseResponse<>(response, Status.success, null, null);
            } else {
                return new BaseResponse<>(response, Status.failure, ErrorCode.getErrorCode(response.getErrorCode()),
                        response.getErrorMessage());
            }

        } catch (Exception e) {
            return new BaseResponse<>(null, Status.failure, ErrorCode.TECHNICAL_ERROR, null);
        }
    }

    @Override
    public BaseResponse<HandlingChargesResponse> getOrderHandlingCharges(List<String> productIds) {
        GiftVoucherProxy proxy = aggregatorFactory.getProxy(Aggregator.WOHOO);
        try {
            HandlingChargesResponse response = proxy.getHandlingCharges(productIds);
            if (response.getStatus()) {
                return new BaseResponse<>(response, Status.success, null, null);
            } else {
                return new BaseResponse<>(response, Status.failure, ErrorCode.getErrorCode(response.getErrorCode()),
                        response.getErrorMessage());
            }
        } catch (Exception e) {
            return new BaseResponse<>(null, Status.failure, ErrorCode.TECHNICAL_ERROR, null);
        }
    }

}
