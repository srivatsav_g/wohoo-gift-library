package com.techtribes.gv.service.bean;

public enum UserType {
    BILLING_USER, SHIPPING_USER
}
