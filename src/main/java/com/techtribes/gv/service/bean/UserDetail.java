package com.techtribes.gv.service.bean;

import java.io.Serializable;

public class UserDetail implements Serializable {

    private static final long serialVersionUID = -3077534456415149709L;
    private Title title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String dob;
    private CommunicationDetail<String> phone;
    private CommunicationDetail<String> landLine;
    private CommunicationDetail<String> email;
    private CommunicationDetail<Address> address;
    private UserType userType;

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public CommunicationDetail<String> getPhone() {
        return phone;
    }

    public void setPhone(CommunicationDetail<String> phone) {
        this.phone = phone;
    }

    public CommunicationDetail<String> getLandLine() {
        return landLine;
    }

    public void setLandLine(CommunicationDetail<String> landLine) {
        this.landLine = landLine;
    }

    public CommunicationDetail<String> getEmail() {
        return email;
    }

    public void setEmail(CommunicationDetail<String> email) {
        this.email = email;
    }

    public CommunicationDetail<Address> getAddress() {
        return address;
    }

    public void setAddress(CommunicationDetail<Address> address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "UserDetail [title=" + title + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
                + lastName + ", dob=" + dob + ", phone=" + phone + ", landLine=" + landLine + ", email=" + email
                + ", address=" + address + ", userType=" + userType + "]";
    }
}
