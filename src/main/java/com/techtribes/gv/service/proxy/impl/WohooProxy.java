package com.techtribes.gv.service.proxy.impl;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techtribes.gv.service.mapper.WohooMapper;
import com.techtribes.gv.service.model.request.PlaceOrderRequest;
import com.techtribes.gv.service.model.response.BaseConfigurationResponse;
import com.techtribes.gv.service.model.response.CategoryDataResponse;
import com.techtribes.gv.service.model.response.CategoryDetailsResponse;
import com.techtribes.gv.service.model.response.HandlingChargesResponse;
import com.techtribes.gv.service.model.response.OrderDataResponse;
import com.techtribes.gv.service.proxy.GiftVoucherProxy;
import com.techtribes.sdk.wohoo.WohooAsyncClient;
import com.techtribes.sdk.wohoo.WohooClientException;
import com.techtribes.sdk.wohoo.model.request.OrderHandlingRequest;
import com.techtribes.sdk.wohoo.model.response.CategoryResponse;
import com.techtribes.sdk.wohoo.model.response.OrderHandlingResponse;
import com.techtribes.sdk.wohoo.model.response.PlaceOrderResponse;
import com.techtribes.sdk.wohoo.model.response.SettingsResponse;
import com.techtribes.sdk.wohoo.resource.PathParamBuilder;
import com.techtribes.sdk.wohoo.resource.PathParams;

@Component
public class WohooProxy implements GiftVoucherProxy {

    @Autowired
    private WohooAsyncClient client;

    @Autowired
    private WohooMapper mapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(WohooProxy.class);

    private final String LOG_PREFIX = this.getClass().getSimpleName();

    @Override
    public BaseConfigurationResponse getSettings() {
        BaseConfigurationResponse response = new BaseConfigurationResponse();
        try {
            SettingsResponse settingsResponse = client.getSettings().get();
            response = mapper.toBaseCofiguation(settingsResponse);

        } catch (InterruptedException | ExecutionException | WohooClientException e) {

            LOGGER.error(LOG_PREFIX + "Exception in Settings API :: " + e);
        }
        return response;
    }

    @Override
    public CategoryDetailsResponse getCategoryDetails() {
        CategoryDetailsResponse response = new CategoryDetailsResponse();

        try {
            CategoryResponse categoryListResponse = client.getCategory().get();
            response = mapper.toCateoryDetailsResponse(categoryListResponse);
        } catch (InterruptedException | ExecutionException | WohooClientException e) {
            LOGGER.error(LOG_PREFIX + "Exception in Category Details API :: " + e);
        }
        return response;
    }

    @Override
    public CategoryDataResponse getCategoryData(String categoryId) {
        CategoryDataResponse response = new CategoryDataResponse();
        try {
            PathParams pathParams = PathParamBuilder.standard().addParam(categoryId).build();
            com.techtribes.sdk.wohoo.model.response.CategoryDataResponse categoryDataResponse =
                    client.getCategoryData(pathParams).get();
            response = mapper.toCategoryDataResponse(categoryDataResponse);
        } catch (InterruptedException | ExecutionException | WohooClientException e) {
            LOGGER.error(LOG_PREFIX + "Exception in Category Data Response API :: " + e);
        }
        return response;
    }

    @Override
    public HandlingChargesResponse getHandlingCharges(List<String> productIds) {
        HandlingChargesResponse response = new HandlingChargesResponse();
        try {
            OrderHandlingRequest orderHandlingRequest = new OrderHandlingRequest();
            orderHandlingRequest.setProducts(productIds);
            OrderHandlingResponse orderHandlingResponse = client.getOrderHandlingCharges(orderHandlingRequest).get();
            response = mapper.toHandlingChargesResponse(orderHandlingResponse);
        } catch (InterruptedException | ExecutionException | WohooClientException e) {
            LOGGER.error(LOG_PREFIX + "Exception in Handling Charges Response API :: " + e);
        }
        return response;
    }

    @Override
    public OrderDataResponse placeOrder(PlaceOrderRequest placeOrderRequest) {
        OrderDataResponse orderDataResponse = new OrderDataResponse();
        try {
            com.techtribes.sdk.wohoo.model.request.PlaceOrderRequest aggregatorOrderrequest =
                    mapper.toAggregatorOrderRequest(placeOrderRequest);
            PlaceOrderResponse orderResponse = client.placeOrder(aggregatorOrderrequest).get();
            orderDataResponse = mapper.toOrderDataResponse(orderResponse);
        } catch (InterruptedException | ExecutionException | WohooClientException e) {
            LOGGER.error(LOG_PREFIX + "Exception in Handling Charges Response API :: " + e);
        }
        return orderDataResponse;
    }

}
