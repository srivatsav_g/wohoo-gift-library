package com.techtribes.sdk.wohoo.resource;

public enum QueryKey {
    SETTINGS("/settings");
    
    private final String identifier;
    
    private QueryKey(String identifier){
        this.identifier = identifier;
    }
    
    public String toString() {
        return this.identifier;
    }
}
