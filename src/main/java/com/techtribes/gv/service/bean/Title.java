package com.techtribes.gv.service.bean;

public enum Title {

    MR, MRS, MS, MSTR, MISS;

}
