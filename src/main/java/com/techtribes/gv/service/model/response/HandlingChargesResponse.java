package com.techtribes.gv.service.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techtribes.gv.response.BaseData;

/**
 * Bean to map handling charges of an order.
 * 
 * @author techmojo
 *
 */
public class HandlingChargesResponse extends BaseData {

    /**
     * Handling charges for an order.
     */
    private Integer hadlingCharges;

    private Boolean Status;

    @JsonIgnore
    private String errorCode;
    
    @JsonIgnore
    private String errorMessage;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getHadlingCharges() {
        return hadlingCharges;
    }

    public void setHadlingCharges(Integer hadlingCharges) {
        this.hadlingCharges = hadlingCharges;
    }

    @Override
    public String toString() {
        return "HandlingChargesResponse [hadlingCharges=" + hadlingCharges + ", Status=" + Status + ", errorCode="
                + errorCode + ", errorMessage=" + errorMessage + "]";
    }

}
