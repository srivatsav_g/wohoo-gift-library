package com.techtribes.gv.service.proxy.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techtribes.gv.service.bean.Aggregator;
import com.techtribes.gv.service.proxy.GiftVoucherProxy;

@Component
public class GiftVoucherProxyFactory {

    @Autowired
    WohooProxy wohooProxy;

    public GiftVoucherProxy getProxy(Aggregator aggregator) {

        switch (aggregator) {
            case WOHOO:
                return wohooProxy;
            default:
                return wohooProxy;
        }
    }

}
