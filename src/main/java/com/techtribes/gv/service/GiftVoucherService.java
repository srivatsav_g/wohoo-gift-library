package com.techtribes.gv.service;

import java.util.List;

import com.techtribes.gv.response.BaseResponse;
import com.techtribes.gv.service.model.request.PlaceOrderRequest;
import com.techtribes.gv.service.model.response.BaseConfigurationResponse;
import com.techtribes.gv.service.model.response.CategoryDataResponse;
import com.techtribes.gv.service.model.response.CategoryDetailsResponse;
import com.techtribes.gv.service.model.response.HandlingChargesResponse;
import com.techtribes.gv.service.model.response.OrderDataResponse;

/**
* 
* <b>Purpose</b> : <br/>
* This service provides implementation for buying a gift voucher. 
* We make use of aggregators like Wohoo (API provider) internally.<br/>
* 
*/
public interface GiftVoucherService {

    /**
     * This is the first step of integration with Wohoo APIs.
     * This step is to get the base configuration of the account like
     * max quantity,terms and conditions,etc..
     * 
     * @return BaseConfigurationResponse
     */
    public BaseResponse<BaseConfigurationResponse> getSettings();
    /**
     * This is the second step of integration where we get the 
     * list of categories and sub-categories that are enabled for our account.
     * 
     * @return CategoryDetailsResponse
     */
    public BaseResponse<CategoryDetailsResponse> getCategoryDetails();
    /**
     * This is the third step of integration where we get the category details
     * and products for a given category Id.
     *  
     * @return CategoryDataResponse
     */
    public BaseResponse<CategoryDataResponse> getCategoryData(String categoryId);

    /**
     * This step in integration is to get the handling charges for a particular
     * order given a list of products. These handling charges are to be added to total redeemable amount
     * before calling spend API i.e {@link GiftVoucherService#placeOrder(PlaceOrderRequest)}.
     *  
     * @param handlingRequest
     * @return
     */
    public BaseResponse<HandlingChargesResponse> getOrderHandlingCharges(List<String> productIds);
    
    /**
     * This is the final step of placing an order for a gift voucher. The user will
     * review the voucher and confirm the roka payment.Then the system does an MPS debit
     * and place the order at the aggregator side. (If mps debit ails, it is refunded.)
     * 
     * @param placeOrderRequest
     * @return
     */
    public BaseResponse<OrderDataResponse> placeOrder(PlaceOrderRequest placeOrderRequest);
}
