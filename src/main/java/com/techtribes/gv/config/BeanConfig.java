package com.techtribes.gv.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import com.techtribes.sdk.wohoo.WohooAsyncClient;
import com.techtribes.sdk.wohoo.WohooAsyncClientBuilder;

@Configuration
// @EnableJpaRepositories
public class BeanConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(BeanConfig.class);

    @Autowired
    Environment env;

    @Value("${aggregators.wohoo.baseURL}")
    private String baseURL;
    @Value("${aggregators.wohoo.consumerKey}")
    private String consumerKey;
    @Value("${aggregators.wohoo.consumerSecret}")
    private String consumerSecret;
    @Value("${aggregators.wohoo.accessToken}")
    private String accessToken;
    @Value("${aggregators.wohoo.accessTokenSecret}")
    private String accessTokenSecret;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public WohooAsyncClient wohooAsyncClient(RestTemplateBuilder builder) {
        return WohooAsyncClientBuilder.standard(builder).endpoint(baseURL).consumerKey(consumerKey)
                .consumerSecret(consumerSecret).accessToken(accessToken).accessTokenSecret(accessTokenSecret)
                .enableRequestLogging(true).enableResponseLogging(true).build();
    }

}
