package com.techtribes.gv.response;

import java.io.Serializable;

import com.techtribes.gv.constants.ErrorCode;
import com.techtribes.gv.constants.Constants.Status;

/**
 * 
 * @author srivatsav
 *
 */
public class BaseResponse<T extends BaseData> implements Serializable {

    private static final long serialVersionUID = 8968959070522249866L;

    private T response;

    private Status status;

    private ErrorCode errorCode;

    private String errorMessage;

    public BaseResponse() {
        super();
    }

    public BaseResponse(T response) {
        super();
        this.response = response;
        this.status = Status.success;
    }

    public BaseResponse(T response, Status status, ErrorCode errorCode, String errorMessage) {
        super();
        this.response = response;
        this.status = status;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BaseResponse [response=");
        builder.append(response);
        builder.append(", status=");
        builder.append(status);
        builder.append(", errorCode=");
        builder.append(errorCode);
        builder.append(", errorMessage=");
        builder.append(errorMessage);
        builder.append("]");
        return builder.toString();
    }

}
