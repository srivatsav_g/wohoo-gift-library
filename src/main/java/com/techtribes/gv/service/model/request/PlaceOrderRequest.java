package com.techtribes.gv.service.model.request;

import java.util.List;

import com.techtribes.gv.service.bean.PaymentType;
import com.techtribes.gv.service.bean.Product;
import com.techtribes.gv.service.bean.UserDetail;

public class PlaceOrderRequest {

    private UserDetail billingUserDetail;

    private UserDetail shippingUserDetail;

    private PaymentType paymentType;

    private String amountToRedeem;

    private List<Product> products;
    /**
     * unique purchase order number for our system.
     */
    private String purchaseOrderNumber;
    /**
     * aggregator reference number
     */
    private String aggreagatorOrderRefNumber;

    
    public String getAggreagatorOrderRefNumber() {
        return aggreagatorOrderRefNumber;
    }

    public void setAggreagatorOrderRefNumber(String aggreagatorOrderRefNumber) {
        this.aggreagatorOrderRefNumber = aggreagatorOrderRefNumber;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public UserDetail getBillingUserDetail() {
        return billingUserDetail;
    }

    public void setBillingUserDetail(UserDetail billingUserDetail) {
        this.billingUserDetail = billingUserDetail;
    }

    public UserDetail getShippingUserDetail() {
        return shippingUserDetail;
    }

    public void setShippingUserDetail(UserDetail shippingUserDetail) {
        this.shippingUserDetail = shippingUserDetail;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getAmountToRedeem() {
        return amountToRedeem;
    }

    public void setAmountToRedeem(String amountToRedeem) {
        this.amountToRedeem = amountToRedeem;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    @Override
    public String toString() {
        return "PlaceOrderRequest [billingUserDetail=" + billingUserDetail + ", shippingUserDetail="
                + shippingUserDetail + ", paymentType=" + paymentType + ", amountToRedeem=" + amountToRedeem
                + ", products=" + products + ", purchaseOrderNumber=" + purchaseOrderNumber
                + ", aggreagatorOrderRefNumber=" + aggreagatorOrderRefNumber + "]";
    }

}
