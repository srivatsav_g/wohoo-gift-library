package com.techtribes.sdk.wohoo.utils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.auth.oauth.OAuthHmacSigner;
import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.http.GenericUrl;

public class RequestSender {

    private RestTemplate restTemplate;
    private RestTemplateLoggingInterceptor loggingInterceptor;
    private ObjectMapper mapper;
    private static final Logger LOG = LoggerFactory.getLogger(RequestSender.class);

    public RequestSender() {
        this.loggingInterceptor = new RestTemplateLoggingInterceptor();
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(loggingInterceptor);

        this.restTemplate =
                new RestTemplate(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
        restTemplate.setInterceptors(interceptors);
        restTemplate.setMessageConverters(getMessageConvertors());
    }

    public RequestSender(RestTemplateBuilder builder) {
        this.loggingInterceptor = new RestTemplateLoggingInterceptor();
        this.restTemplate = buildRestTemplate(builder, loggingInterceptor);
    }

    public void enableRequestLogging(Boolean value) {
        this.loggingInterceptor.enableRequestBodyLogging(value);
    }

    public void enableResponseLogging(Boolean value) {
        this.loggingInterceptor.enableResponseBodyLogging(value);
    }

    private List<HttpMessageConverter<?>> getMessageConvertors() {
        return Arrays.asList(getJSONMessageConvertor());
    }

    private HttpMessageConverter<?> getJSONMessageConvertor() {
        MappingJackson2HttpMessageConverter jsonConvertor = new MappingJackson2HttpMessageConverter();
        jsonConvertor
                .setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON_UTF8));
        return jsonConvertor;
    }

    private RestTemplate buildRestTemplate(RestTemplateBuilder builder, RestTemplateLoggingInterceptor interceptor) {
        return builder.requestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()))
                .interceptors(loggingInterceptor).messageConverters(getMessageConvertors()).build();
    }

    public <T> T httpGet(String url, Map<String, String> headers, Class<T> responseType) {

        HttpHeaders httpHeaders = new HttpHeaders();
        String authHeaderValue = computeAuthorizationHeader(url, headers, HttpMethod.GET.name());
        httpHeaders.add("Authorization", authHeaderValue);

        try {

            return (T) restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(httpHeaders), responseType)
                    .getBody();
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            mapper = new ObjectMapper();
            try {
                return (T) mapper.readValue(e.getResponseBodyAsString(), responseType);
            } catch (IOException e1) {
                LOG.error("Error in mapping error response body to response type :: " + e1);
                return null;
            }
        }

    }

    public <T> T httpPost(String url, Map<String, String> headers, Object body, Class<T> responseType) {

        HttpHeaders httpHeaders = new HttpHeaders();
        String authHeaderValue = computeAuthorizationHeader(url, headers, HttpMethod.POST.name());
        httpHeaders.add("Authorization", authHeaderValue);

        try {
            return (T) restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(body, httpHeaders), responseType)
                    .getBody();
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            mapper = new ObjectMapper();
            try {
                return (T) mapper.readValue(e.getResponseBodyAsString(), responseType);
            } catch (IOException e1) {
                LOG.error("Error in mapping error response body to response type :: ", e1);
                return null;
            }
        }

    }

    public <T> T httpDelete(String url, Map<String, String> headers, Class<T> responseType) {

        HttpHeaders httpHeaders = new HttpHeaders();
        String authHeaderValue = computeAuthorizationHeader(url, headers, HttpMethod.DELETE.name());
        httpHeaders.add("Authorization", authHeaderValue);
        try {
            return (T) restTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(httpHeaders), responseType)
                    .getBody();
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            mapper = new ObjectMapper();
            try {
                return (T) mapper.readValue(e.getResponseBodyAsString(), responseType);
            } catch (IOException e1) {
                LOG.error("Error in mapping error response body to response type :: ", e1);
                return null;
            }
        }

    }

    /**
     * Method to build OAuth Headers.
     * 
     * @param url
     * @param headers
     * @param methodName
     * @return
     */
    private String computeAuthorizationHeader(String url, Map<String, String> headers, String methodName) {

        final OAuthHmacSigner signer = new OAuthHmacSigner();
        signer.clientSharedSecret = headers.get("consumer_secret");
        signer.tokenSharedSecret = headers.get("access_token_secret");
        String signatureMethod = signer.getSignatureMethod();
        final OAuthParameters oauthParameters = new OAuthParameters();
        oauthParameters.consumerKey = headers.get("consumer_key");
        oauthParameters.token = headers.get("access_token");
        oauthParameters.signer = signer;
        oauthParameters.version = "1.0";
        oauthParameters.signatureMethod = signatureMethod;
        oauthParameters.computeNonce();
        oauthParameters.computeTimestamp();
        final GenericUrl genericRequestUrl = new GenericUrl(url);
        try {
            oauthParameters.computeSignature(methodName, genericRequestUrl);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
        LOG.info("Request Headers :: " + oauthParameters.getAuthorizationHeader());
        return oauthParameters.getAuthorizationHeader();
    }

}
