package com.techtribes.gv.service.bean;

public enum PriceType {

    SLAB, RANGE;

    public static PriceType valueof(String priceType) {
        for (PriceType p : PriceType.values()) {
            if (p.name().equalsIgnoreCase(priceType.trim())) {
                return p;
            }
        }
        return null;
    }
}
