package com.techtribes.sdk.wohoo;

import java.util.concurrent.Future;

import com.techtribes.sdk.wohoo.model.request.OrderHandlingRequest;
import com.techtribes.sdk.wohoo.model.request.PlaceOrderRequest;
import com.techtribes.sdk.wohoo.model.response.CategoryDataResponse;
import com.techtribes.sdk.wohoo.model.response.CategoryResponse;
import com.techtribes.sdk.wohoo.model.response.OrderHandlingResponse;
import com.techtribes.sdk.wohoo.model.response.OrderRefnoResponse;
import com.techtribes.sdk.wohoo.model.response.PlaceOrderResponse;
import com.techtribes.sdk.wohoo.model.response.ProductDetailsResponse;
import com.techtribes.sdk.wohoo.model.response.SettingsResponse;
import com.techtribes.sdk.wohoo.resource.PathParams;

public interface WohooAsync {

    public Future<SettingsResponse> getSettings() throws WohooClientException;

    public Future<CategoryResponse> getCategory() throws WohooClientException;

    public Future<CategoryDataResponse> getCategoryData(PathParams pathParams) throws WohooClientException;

    public Future<ProductDetailsResponse> getProductDetails(PathParams pathParams) throws WohooClientException;

    public Future<PlaceOrderResponse> placeOrder(PlaceOrderRequest placeOrderRequest) throws WohooClientException;

    public Future<OrderHandlingResponse> getOrderHandlingCharges(OrderHandlingRequest orderHAndlingRequest)
            throws WohooClientException;

    public Future<OrderRefnoResponse> getOrderReferenceNumber() throws WohooClientException;
}
