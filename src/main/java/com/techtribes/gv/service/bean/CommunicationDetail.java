package com.techtribes.gv.service.bean;

import java.io.Serializable;

public class CommunicationDetail<T> implements Serializable {

    private static final long serialVersionUID = -1985806888294042343L;
    private CommunicationType type;
    private T detail;

    public CommunicationType getType() {
        return type;
    }

    public void setType(CommunicationType type) {
        this.type = type;
    }

    public T getDetail() {
        return detail;
    }

    public void setDetail(T detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "CommunicationDetail [type=" + type + ", detail=" + detail + "]";
    }
}
