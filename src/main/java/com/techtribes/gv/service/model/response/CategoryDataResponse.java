package com.techtribes.gv.service.model.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techtribes.gv.response.BaseData;
import com.techtribes.gv.service.bean.Category;
import com.techtribes.gv.service.bean.Product;

public class CategoryDataResponse extends BaseData {

    private Boolean status;
    private Category category;
    @JsonIgnore
    private String categoryUrl;
    /**
     * url path for category description.
     */
    private String navigationUrlPath;
    private String categoryImage;
    /**
     * sub categories of the category.
     */
    private List<Category> subCategories;
    /**
     * products enabled under the category.
     */
    private List<Product> products;

    @JsonIgnore
    private String errorCode;
    @JsonIgnore
    private String errorMessage;

    public Boolean getStatus() {
        return status;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public String getNavigationUrlPath() {
        return navigationUrlPath;
    }

    public void setNavigationUrlPath(String navigationUrlPath) {
        this.navigationUrlPath = navigationUrlPath;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<Category> subCategories) {
        this.subCategories = subCategories;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "CategoryDataResponse [status=" + status + ", category=" + category + ", categoryUrl=" + categoryUrl
                + ", navigationUrlPath=" + navigationUrlPath + ", categoryImage=" + categoryImage + ", subCategories="
                + subCategories + ", products=" + products + ", errorCode=" + errorCode + ", errorMessage="
                + errorMessage + "]";
    }

}
