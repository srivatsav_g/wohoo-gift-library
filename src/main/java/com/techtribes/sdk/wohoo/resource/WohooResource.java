package com.techtribes.sdk.wohoo.resource;

public enum WohooResource implements Resource {

    SETTINGS("/settings"),
    CATEGORY("/category"),
    CATEGORY_DATA("/category"),
    PRODUCT_DETAILS("/product"),
    REFERENCE_NUMBER("/refno"),
    ORDER_HANDLING("/orderhandling"),
    SPEND("/spend"),
    ORDER_STATUS("/status"),
    CHECK_BALANCE("/checkbalance"),
    RESEND_API("/resend");

    private final String uri;

    WohooResource(String uri) {
        this.uri = uri;
    }

    @Override
    public String getUri() {
        return uri;
    }
}
