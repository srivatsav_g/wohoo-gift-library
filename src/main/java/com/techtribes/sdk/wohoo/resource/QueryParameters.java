package com.techtribes.sdk.wohoo.resource;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class QueryParameters {

    private List<SimpleFilter> simpleFilters;

    public QueryParameters() {
        this.simpleFilters = new LinkedList<>();
    }

    public void addSimpleFilter(SimpleFilter simpleFilters) {
        this.simpleFilters.add(simpleFilters);
    }

    public void addSimpleFilters(SimpleFilter... simpleFilters) {
        this.simpleFilters.addAll(Arrays.asList(simpleFilters));
    }

    private String getSimpleFiltersQueryString() {
        return simpleFilters.stream()
                .map(simpleFilter -> String.format("%s=%s", simpleFilter.getKey(), simpleFilter.getValue()))
                .collect(Collectors.joining("&"));
    }

    /**
     * Returns a string appropriate for inclusion in an HTTP request including all filters added.
     */
    public String toString() {
        return new StringBuilder().append(getSimpleFiltersQueryString()).toString();
    }
}