package com.techtribes.gv.service.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techtribes.gv.response.BaseResponse;
import com.techtribes.gv.service.GiftVoucherService;
import com.techtribes.gv.service.model.response.BaseConfigurationResponse;
import com.techtribes.gv.service.model.response.CategoryDataResponse;
import com.techtribes.gv.service.model.response.CategoryDetailsResponse;

@RestController
@RequestMapping({"/gift-voucher-service"})
public class GiftVoucherController {

    private static Logger LOGGER = LoggerFactory.getLogger(GiftVoucherController.class);

    private final String LOG_PREFIX = this.getClass().getName();
    @Autowired
    private GiftVoucherService giftVoucherService;

    @RequestMapping(value = {"/getAccountConfiguration"}, method = {RequestMethod.GET})
    public BaseResponse<BaseConfigurationResponse> getAccountConfiguration() {
        LOGGER.info(LOG_PREFIX+":: getAccountConfiguration");
        return giftVoucherService.getSettings();
    }
    
    @RequestMapping(value="/getCategories",method=RequestMethod.GET)
    public BaseResponse<CategoryDetailsResponse> getCategories(){
        LOGGER.info(LOG_PREFIX+":: getCategories");
        return giftVoucherService.getCategoryDetails();
    }
    
    @RequestMapping(value="/getCategoryDetails",method=RequestMethod.GET)
    public BaseResponse<CategoryDataResponse> getCategoryDetails(@RequestParam String categoryId){
        LOGGER.info(LOG_PREFIX+":: getCategoryDetails");
        return giftVoucherService.getCategoryData(categoryId);
    }
}
