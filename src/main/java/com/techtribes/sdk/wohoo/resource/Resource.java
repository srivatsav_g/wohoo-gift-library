package com.techtribes.sdk.wohoo.resource;

public interface Resource {
    String getUri();
}
