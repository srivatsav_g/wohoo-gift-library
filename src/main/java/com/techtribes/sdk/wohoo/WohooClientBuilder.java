package com.techtribes.sdk.wohoo;

import org.springframework.boot.web.client.RestTemplateBuilder;

import com.techtribes.sdk.wohoo.utils.RequestSender;

public class WohooClientBuilder {


    WohooClient client;

    public WohooClientBuilder(RestTemplateBuilder builder) {
        this.client = new WohooClient(new RequestSender(builder));
    }

    public WohooClientBuilder() {
        this.client = new WohooClient(new RequestSender());
    }

    public static WohooClientBuilder standard(RestTemplateBuilder builder) {
        return new WohooClientBuilder(builder);
    }

    public static WohooClientBuilder standard() {
        return new WohooClientBuilder();
    }

    public WohooClientBuilder endpoint(String endpoint) {
        this.client.setEndpoint(endpoint);
        return this;
    }

    public WohooClientBuilder enableRequestLogging(Boolean value) {
        this.client.enableRequestLogging(value);
        return this;
    }

    public WohooClientBuilder enableResponseLogging(Boolean value) {
        this.client.enableResponseLogging(value);
        return this;
    }

    public WohooClient build() {
        return this.client;
    }
}
