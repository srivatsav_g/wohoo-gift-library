package com.techtribes.gv.service.bean;

public enum CommunicationType {

    PERSONAL_MOBILE, OFFICE_MOBILE, HOME_LANDLINE, OFFICE_LANDLINE, EMAIL, FAX, POST, WEBSITE;

}
