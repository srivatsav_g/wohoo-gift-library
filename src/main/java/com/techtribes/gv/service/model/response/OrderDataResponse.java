package com.techtribes.gv.service.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techtribes.gv.constants.Constants.OrderStatus;
import com.techtribes.gv.response.BaseData;

public class OrderDataResponse extends BaseData {

    private OrderStatus orderStatus;

    private Boolean isOrderPlaced;

    private String aggregatorOrderId;

    private String aggregatorOrderMessage;

    @JsonIgnore
    private String errorCode;

    @JsonIgnore
    private String errorMessage;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Boolean getIsOrderPlaced() {
        return isOrderPlaced;
    }

    public void setIsOrderPlaced(Boolean isOrderPlaced) {
        this.isOrderPlaced = isOrderPlaced;
    }

    public String getAggregatorOrderId() {
        return aggregatorOrderId;
    }

    public void setAggregatorOrderId(String aggregatorOrderId) {
        this.aggregatorOrderId = aggregatorOrderId;
    }

    public String getAggregatorOrderMessage() {
        return aggregatorOrderMessage;
    }

    public void setAggregatorOrderMessage(String aggregatorOrderMessage) {
        this.aggregatorOrderMessage = aggregatorOrderMessage;
    }

    @Override
    public String toString() {
        return "OrderDataResponse [orderStatus=" + orderStatus + ", isOrderPlaced=" + isOrderPlaced
                + ", aggregatorOrderId=" + aggregatorOrderId + ", aggregatorOrderMessage=" + aggregatorOrderMessage
                + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
    }

}
