package com.techtribes.gv.service.mapper;

import com.techtribes.gv.service.model.response.BaseConfigurationResponse;
import com.techtribes.gv.service.model.response.CategoryDataResponse;
import com.techtribes.gv.service.model.response.CategoryDetailsResponse;
import com.techtribes.gv.service.model.response.HandlingChargesResponse;
import com.techtribes.gv.service.model.response.OrderDataResponse;
import com.techtribes.sdk.wohoo.model.request.PlaceOrderRequest;
import com.techtribes.sdk.wohoo.model.response.CategoryResponse;
import com.techtribes.sdk.wohoo.model.response.OrderHandlingResponse;
import com.techtribes.sdk.wohoo.model.response.PlaceOrderResponse;
import com.techtribes.sdk.wohoo.model.response.SettingsResponse;

public interface WohooMapper {

    /**
     * Method to map settings response to BaseCionfigurationResponse.
     * 
     * @param settingsResponse
     * @return
     */
    public BaseConfigurationResponse toBaseCofiguation(SettingsResponse settingsResponse);

    /**
     * Method to map Category Response with Root,main and child categories to CategoryDetails Response.
     * 
     * 
     * @param categoryListResponse
     * @return
     */
    public CategoryDetailsResponse toCateoryDetailsResponse(CategoryResponse categoryListResponse);

    /**
     * Method to map Category details of a particular category.
     * 
     * @param categoryDataResponse
     * @return
     */
    public CategoryDataResponse toCategoryDataResponse(
            com.techtribes.sdk.wohoo.model.response.CategoryDataResponse categoryDataResponse);

    /**
     * Method to map handling charges of an order.
     * 
     * @param orderHandlingResponse
     * @return
     */
    public HandlingChargesResponse toHandlingChargesResponse(OrderHandlingResponse orderHandlingResponse);

    /**
     * Method to map Order Request before placing an order.
     * 
     * @param placeOrderRequest
     * @return
     */
    public PlaceOrderRequest toAggregatorOrderRequest(
            com.techtribes.gv.service.model.request.PlaceOrderRequest placeOrderRequest);

    /**
     * Method to map place order response post placing an order.
     * 
     * @param placeOrderResponse
     * @return
     */
    public OrderDataResponse toOrderDataResponse(PlaceOrderResponse placeOrderResponse);
}
