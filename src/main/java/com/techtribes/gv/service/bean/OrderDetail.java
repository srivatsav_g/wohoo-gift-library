package com.techtribes.gv.service.bean;

import java.util.List;

public class OrderDetail {

    private Long orderHandlingCharges;

    private UserDetail bookingUserDetail;

    private PaymentType paymentType;

    private Long totalRedeemAmount;

    /**
     * Unique po number in our system.
     */
    private String purchaseOrderNumber;
    /**
     * generated using /refno API.
     */
    private String aggregatorOrderRefNumber;

    private String aggregatorOrderId;

    private List<Product> productDetails;

    public Long getOrderHandlingCharges() {
        return orderHandlingCharges;
    }

    public void setOrderHandlingCharges(Long orderHandlingCharges) {
        this.orderHandlingCharges = orderHandlingCharges;
    }

    public UserDetail getBookingUserDetail() {
        return bookingUserDetail;
    }

    public void setBookingUserDetail(UserDetail bookingUserDetail) {
        this.bookingUserDetail = bookingUserDetail;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Long getTotalRedeemAmount() {
        return totalRedeemAmount;
    }

    public void setTotalRedeemAmount(Long totalRedeemAmount) {
        this.totalRedeemAmount = totalRedeemAmount;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getAggregatorOrderRefNumber() {
        return aggregatorOrderRefNumber;
    }

    public void setAggregatorOrderRefNumber(String aggregatorOrderRefNumber) {
        this.aggregatorOrderRefNumber = aggregatorOrderRefNumber;
    }

    public String getAggregatorOrderId() {
        return aggregatorOrderId;
    }

    public void setAggregatorOrderId(String aggregatorOrderId) {
        this.aggregatorOrderId = aggregatorOrderId;
    }

    public List<Product> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(List<Product> productDetails) {
        this.productDetails = productDetails;
    }

    @Override
    public String toString() {
        return "OrderDetail [orderHandlingCharges=" + orderHandlingCharges + ", bookingUserDetail=" + bookingUserDetail
                + ", paymentType=" + paymentType + ", totalRedeemAmount=" + totalRedeemAmount + ", purchaseOrderNumber="
                + purchaseOrderNumber + ", aggregatorOrderRefNumber=" + aggregatorOrderRefNumber
                + ", aggregatorOrderId=" + aggregatorOrderId + ", productDetails=" + productDetails + "]";
    }

}
