package com.techtribes.gv.response;

import com.techtribes.gv.service.bean.Aggregator;

public class BaseData {

    private Aggregator aggregator = Aggregator.WOHOO;

    private String agent;

    private String comments;

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Aggregator getAggregator() {
        return aggregator;
    }

    public void setAggregator(Aggregator aggregator) {
        this.aggregator = aggregator;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BaseData [aggregator=");
        builder.append(aggregator);
        builder.append(", agent=");
        builder.append(agent);
        builder.append(", comments=");
        builder.append(comments);
        builder.append("]");
        return builder.toString();
    }

}
