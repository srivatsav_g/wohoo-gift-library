package com.techtribes.gv.service.bean;

import java.util.List;
import java.util.Map;

public class Product {

    private String aggregatorProductId;

    private String aggregatorBrandId;

    private String aggregatorBrandName;

    private String thumbnailImage;

    private String wideImage;

    private Long minPrice;

    private Long maxPrice;

    private String currency;

    private String productType;

    private String giftMessage;
    /**
     * denominations in 200/-,250/-
     */
    private String denominations;
    /**
     * terms and conditions for web with style.
     */
    private String webterms;
    /**
     * terms and conditions for mobile with style.
     */
    private String mobileTerms;
    /**
     * (k,v) -> (teaher's day theme -> [{theme1.jpeg},{theme2.jpeg}])
     */
    private Map<String, List<String>> themes;

    private Long productPrice;

    private Integer productQuantity;

    private PriceType priceType;

    public Long getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Long productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getAggregatorBrandId() {
        return aggregatorBrandId;
    }

    public void setAggregatorBrandId(String aggregatorBrandId) {
        this.aggregatorBrandId = aggregatorBrandId;
    }

    public String getAggregatorBrandName() {
        return aggregatorBrandName;
    }

    public void setAggregatorBrandName(String aggregatorBrandName) {
        this.aggregatorBrandName = aggregatorBrandName;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getWideImage() {
        return wideImage;
    }

    public void setWideImage(String wideImage) {
        this.wideImage = wideImage;
    }

    public Long getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Long minPrice) {
        this.minPrice = minPrice;
    }

    public Long getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Long maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAggregatorProductId() {
        return aggregatorProductId;
    }

    public void setAggregatorProductId(String aggregatorProductId) {
        this.aggregatorProductId = aggregatorProductId;
    }

    public String getDenominations() {
        return denominations;
    }

    public void setDenominations(String denominations) {
        this.denominations = denominations;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getWebterms() {
        return webterms;
    }

    public void setWebterms(String webterms) {
        this.webterms = webterms;
    }

    public String getMobileTerms() {
        return mobileTerms;
    }

    public void setMobileTerms(String mobileTerms) {
        this.mobileTerms = mobileTerms;
    }

    public Map<String, List<String>> getThemes() {
        return themes;
    }

    public void setThemes(Map<String, List<String>> themes) {
        this.themes = themes;
    }

    public String getGiftMessage() {
        return giftMessage;
    }

    public void setGiftMessage(String giftMessage) {
        this.giftMessage = giftMessage;
    }

    public PriceType getPriceType() {
        return priceType;
    }

    public void setPriceType(PriceType priceType) {
        this.priceType = priceType;
    }

    @Override
    public String toString() {
        return "Product [aggregatorProductId=" + aggregatorProductId + ", aggregatorBrandId=" + aggregatorBrandId
                + ", aggregatorBrandName=" + aggregatorBrandName + ", thumbnailImage=" + thumbnailImage + ", wideImage="
                + wideImage + ", minPrice=" + minPrice + ", maxPrice=" + maxPrice + ", currency=" + currency
                + ", productType=" + productType + ", giftMessage=" + giftMessage + ", denominations=" + denominations
                + ", webterms=" + webterms + ", mobileTerms=" + mobileTerms + ", themes=" + themes + ", productPrice="
                + productPrice + ", productQuantity=" + productQuantity + ", priceType=" + priceType + "]";
    }

}
