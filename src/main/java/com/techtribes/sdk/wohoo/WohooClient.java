package com.techtribes.sdk.wohoo;

import java.util.HashMap;
import java.util.Map;

import com.techtribes.sdk.wohoo.model.request.OrderHandlingRequest;
import com.techtribes.sdk.wohoo.model.response.CategoryResponse;
import com.techtribes.sdk.wohoo.model.response.OrderHandlingResponse;
import com.techtribes.sdk.wohoo.model.response.OrderRefnoResponse;
import com.techtribes.sdk.wohoo.model.response.PlaceOrderResponse;
import com.techtribes.sdk.wohoo.model.response.ProductDetailsResponse;
import com.techtribes.sdk.wohoo.model.response.SettingsResponse;
import com.techtribes.sdk.wohoo.resource.PathParams;
import com.techtribes.sdk.wohoo.resource.WohooResource;
import com.techtribes.sdk.wohoo.utils.RequestSender;

public class WohooClient implements Wohoo {

    RequestSender requestSender;

    private String endpoint;

    private String consumerKey;

    private String consumerSecret;

    private String accessToken;

    private String accessTokenSecret;

    public WohooClient(RequestSender requestSender) {
        this.requestSender = requestSender;
    }

    public RequestSender getRequestSender() {
        return requestSender;
    }

    public void setRequestSender(RequestSender requestSender) {
        this.requestSender = requestSender;
    }

    public void enableResponseLogging(Boolean value) {
        this.requestSender.enableResponseLogging(value);
    }

    public void enableRequestLogging(Boolean value) {
        this.requestSender.enableRequestLogging(value);
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }

    @Override
    public SettingsResponse getSettings() {
        return requestSender.httpGet(buildUrl(endpoint, WohooResource.SETTINGS, null, null), getAuthHeaders(),
                SettingsResponse.class);
    }

    @Override
    public CategoryResponse getCategory() {
        return requestSender.httpGet(buildUrl(endpoint, WohooResource.CATEGORY, null, null), getAuthHeaders(),
                CategoryResponse.class);
    }

    @Override
    public com.techtribes.sdk.wohoo.model.response.CategoryDataResponse getCategoryData(PathParams pathParams) {
        return requestSender.httpGet(buildUrl(endpoint, WohooResource.CATEGORY_DATA, pathParams, null),
                getAuthHeaders(), com.techtribes.sdk.wohoo.model.response.CategoryDataResponse.class);
    }

    @Override
    public ProductDetailsResponse getProductDetails(PathParams pathParams) {
        return requestSender.httpGet(buildUrl(endpoint, WohooResource.PRODUCT_DETAILS, pathParams, null),
                getAuthHeaders(), ProductDetailsResponse.class);
    }

    @Override
    public PlaceOrderResponse placeOrder(com.techtribes.sdk.wohoo.model.request.PlaceOrderRequest orderRequest) {
        return requestSender.httpPost(buildUrl(endpoint, WohooResource.SPEND, null, null), getAuthHeaders(),
                orderRequest, PlaceOrderResponse.class);
    }

    @Override
    public OrderHandlingResponse getOrderHandlingCharges(OrderHandlingRequest orderHandlingRequest) {
        return requestSender.httpPost(buildUrl(endpoint, WohooResource.ORDER_HANDLING, null, null), getAuthHeaders(),
                orderHandlingRequest, OrderHandlingResponse.class);
    }

    @Override
    public OrderRefnoResponse getOrderReferenceNumber() {
        return requestSender.httpGet(buildUrl(endpoint, WohooResource.REFERENCE_NUMBER), getAuthHeaders(),
                OrderRefnoResponse.class);
    }

    private Map<String, String> getAuthHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("consumer_key", consumerKey);
        headers.put("consumer_secret", consumerSecret);
        headers.put("access_token", accessToken);
        headers.put("access_token_secret", accessTokenSecret);
        return headers;
    }

}
