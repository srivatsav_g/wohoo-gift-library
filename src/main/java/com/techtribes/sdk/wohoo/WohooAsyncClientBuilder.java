package com.techtribes.sdk.wohoo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import com.techtribes.sdk.wohoo.utils.MDCAwareThreadPoolExecutor;

public class WohooAsyncClientBuilder {

    WohooAsyncClient client;

    public WohooAsyncClientBuilder() {
        this.client = new WohooAsyncClient(
                (ThreadPoolExecutor) newFixedThreadPool(4, new CustomizableThreadFactory("wohoo-sdk-")));
    }

    public WohooAsyncClientBuilder(RestTemplateBuilder builder) {
        this.client = new WohooAsyncClient(builder,
                (ThreadPoolExecutor) newFixedThreadPool(4, new CustomizableThreadFactory("wohoo-sdk-")));
    }

    public static WohooAsyncClientBuilder standard(RestTemplateBuilder builder) {
        return new WohooAsyncClientBuilder(builder);
    }

    public static WohooAsyncClientBuilder standard() {
        return new WohooAsyncClientBuilder();
    }

    public WohooAsyncClientBuilder endpoint(String endpoint) {
        this.client.setEndpoint(endpoint);
        return this;
    }

    public WohooAsyncClientBuilder consumerKey(String consumerKey) {
        this.client.setConsumerKey(consumerKey);
        return this;
    }

    public WohooAsyncClientBuilder consumerSecret(String consumerSecret) {
        this.client.setConsumerSecret(consumerSecret);
        return this;
    }

    public WohooAsyncClientBuilder accessToken(String accessToken) {
        this.client.setAccessToken(accessToken);
        return this;
    }

    public WohooAsyncClientBuilder accessTokenSecret(String accessTokenSecret) {
        this.client.setAccessTokenSecret(accessTokenSecret);
        return this;
    }

    public WohooAsyncClientBuilder enableRequestLogging(Boolean value) {
        this.client.enableRequestLogging(value);
        return this;
    }

    public WohooAsyncClientBuilder enableResponseLogging(Boolean value) {
        this.client.enableResponseLogging(value);
        return this;
    }

    public WohooAsyncClientBuilder corePoolSize(int minThreads) {
        this.client.setCorePoolSize(minThreads);
        return this;
    }

    public WohooAsyncClient build() {
        return client;
    }

    /**
     * A clone of the Executors.newFixedThreadPool helper method, but using an MDC aware ExecutorService to maintain the
     * originating threads MDC context on the new thread.
     * 
     * Note that if the thread pool size is set to 1 odd behaviour ensues, a new thread is created on each execution...
     * 
     * @param numThreads
     * @param threadFactory
     * @return
     */
    static ExecutorService newFixedThreadPool(int numThreads, ThreadFactory threadFactory) {
        return new MDCAwareThreadPoolExecutor(numThreads, numThreads, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(), threadFactory);
    }
}
