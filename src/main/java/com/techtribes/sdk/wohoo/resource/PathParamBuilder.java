package com.techtribes.sdk.wohoo.resource;

import java.util.ArrayList;
import java.util.Arrays;

public class PathParamBuilder {

    private final PathParams pathParams;

    public PathParamBuilder() {
        this.pathParams = new PathParams();
    }

    public static PathParamBuilder standard() {
        return new PathParamBuilder();
    }

    public PathParamBuilder addParam(String param) {

        if (param == null) {
            return this;
        }
        this.pathParams.addParam(param);
        return this;
    }

    public PathParamBuilder addParams(String... params) {

        if (params == null) {
            return this;
        }

        (new ArrayList<String>(Arrays.asList(params))).stream().filter(param -> param == null)
                .forEach(param -> this.pathParams.addParam(param));

        return this;
    }

    public PathParams build() {
        return this.pathParams;
    }
}
